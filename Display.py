from tkinter import Tk, Canvas
from Moover import Moover
from configs import screen_width, screen_height, space_color


class Display:
    def __init__(self, world):

        self.world = world

        self.running = False
        self.scale = 9e7
        self.center = (screen_width / 2, screen_height / 2)

        self.window = Tk()
        self.window.title("Gravity Simulator")

        self.window.protocol("WM_DELETE_WINDOW", self.kill)
        self.window.bind('<Escape>', self.kill)
        self.window.bind('<Button-4>', self.zoom_in)
        self.window.bind('<Button-5>', self.zoom_out)

        self.canvas = Canvas(self.window, width=screen_width, height=screen_height, bg=space_color)
        self.canvas.pack(fill="both", expand=True)

        moover = Moover(self.window)
        moover.bind(self.moove)

    def run(self):
        self.running = True
        while self.running:
            self.canvas.delete("all")
            for planet in self.world.planets:
                planet.display(self.canvas, self.scale, self.center)
            self.window.update()

    def kill(self, *args):
        self.world.stop()
        self.running = False
        self.window.destroy()

    def zoom_out(self, *args):
        self.scale += 1e6

    def zoom_in(self, *args):
        self.scale -= 1e6
        if self.scale < 1e6:
            self.scale = 1e6

    def moove(self, coords):
        x = self.center[0]+coords[0]
        y = self.center[1]+coords[1]
        self.center = (x, y)
