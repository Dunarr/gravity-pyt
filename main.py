from Display import Display
from World import World
from threading import Thread

if __name__ == "__main__":
    w = World()
    Thread(target=w.start).start()
    d = Display(w)
    d.run()
